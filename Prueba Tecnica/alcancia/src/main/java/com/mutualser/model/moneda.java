package com.mutualser.model;
//Nuestra entidad moneda, corresponde a la tabla que se crea en la base de datos springboot
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class moneda {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer Id;
private Integer Valor;
public Integer getId() {
	return Id;
}
public void setId(Integer id) {
	Id = id;
}
public Integer getValor() {
	return Valor;
}
public void setValor(Integer valor) {
	Valor = valor;
}

}
