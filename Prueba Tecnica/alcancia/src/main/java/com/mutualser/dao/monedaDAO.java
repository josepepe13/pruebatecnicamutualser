package com.mutualser.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.mutualser.model.moneda;
//Aquí creamos los query personalizados
public interface monedaDAO extends JpaRepository<moneda, Integer> {

	@Query("SELECT SUM(m.Valor) FROM moneda m")
	long total();
	
	@Query("SELECT SUM(m.Valor) FROM moneda m where m.Valor=?1")
	long totalenmonedasde(Integer value);
}
