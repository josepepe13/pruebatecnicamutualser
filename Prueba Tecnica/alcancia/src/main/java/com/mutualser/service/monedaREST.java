package com.mutualser.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mutualser.dao.monedaDAO;
import com.mutualser.model.moneda;

@RestController
@RequestMapping("monedas")
public class monedaREST {
@Autowired
private monedaDAO MonedaDAO;

//Guardar moneda en la alcancía
@PostMapping("/guardar")
public ResponseEntity<?> GuardarMoneda(@RequestBody moneda Moneda)
{
	if(Moneda.getValor()!=50 & Moneda.getValor()!=100 & Moneda.getValor()!=200 & Moneda.getValor()!=500 & Moneda.getValor()!=1000)
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Moneda Incorrecta");
		MonedaDAO.save(Moneda);	
		return new ResponseEntity<>(Moneda,HttpStatus.OK);
	
}

//Saber cuantas monedas hay en la alcancía
@GetMapping("/contarmonedas")
public long ContarMonedas()
{
return MonedaDAO.count();	
}

//Saber cuantas monedas de x denominación hay en la alcancía
@PostMapping("/contarmonedasde")
public ResponseEntity<?> ContarMonedasDe(@RequestBody moneda valor)
{
//	Validamos que la moneda consultada sea correcta
	if(valor.getValor()!=50 & valor.getValor()!=100 & valor.getValor()!=200 & valor.getValor()!=500 & valor.getValor()!=1000)
	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Moneda Incorrecta");
return new ResponseEntity<>(MonedaDAO.count(Example.of(valor)),HttpStatus.OK);
}

//Saber cuanto dinero hay en la alcancía
@GetMapping("/total")
public long Total()
{
return MonedaDAO.total();	
}

//Saber cuanto dinero en monedas de x denominación hay en la alcancía
@PostMapping("/totalenmonedasde")
public ResponseEntity<?> TotalEnMonedasDe(@RequestBody moneda Moneda)
{
//	Validamos que la moneda consultada sea correcta
	if(Moneda.getValor()!=50 & Moneda.getValor()!=100 & Moneda.getValor()!=200 & Moneda.getValor()!=500 & Moneda.getValor()!=1000)
	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Moneda Incorrecta");
	
	return new ResponseEntity<>(MonedaDAO.totalenmonedasde(Moneda.getValor()),HttpStatus.OK);
}
}
