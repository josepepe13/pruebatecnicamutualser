package com.mutualser;

import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.Client.CallRestService;
import com.model.moneda;

@SpringBootApplication
public class ConsumeAlcanciaApplication {
//Clase principal: Aquí se codifica el Front y se instancia nuestro cliente REST para consumir el servicio alcancia.
	
	public static void main(String[] args) {
		SpringApplication.run(ConsumeAlcanciaApplication.class, args);
		CallRestService client = new CallRestService();
		Scanner scanner = new Scanner(System.in);
		Boolean exit = false;
		while(!exit) 
		{
		System.out.println("----------------------INICIO---------------------");
		System.out.println("¿Que desea hacer?");
		System.out.println("1. Guardar moneda en la alcancía");
		System.out.println("2. Saber cuantas monedas hay en la alcancía");
		System.out.println("3. Saber cuantas monedas de x denominación hay en la alcancía");
		System.out.println("4. Saber cuanto dinero hay en la alcancía");
		System.out.println("5. Saber cuanto dinero en monedas de x denominación hay en la alcancía");
		System.out.println("6. Salir");
		String entrada = scanner.next();
		switch (entrada)
		{
		case "1":
			System.out.println("Digite el valor de la moneda: ");
			String value = scanner.next();
			Integer intvalue = 0;
			try {
		       intvalue=Integer.parseInt(value);
		    } catch (NumberFormatException e) {
		         intvalue = 0;
		    }
			moneda coin = new moneda();
			coin.setValor(intvalue);
			String ans = client.GuardarMoneda(coin);
			if(ans.contains("Moneda Incorrecta"))
			System.out.println(ans);
			else
				System.out.println("Moneda Guardada");	
			break;
			
		case "2":
			String ans2 = "Existen "+client.ContarMonedas()+" monedas en la alcancía";
			System.out.println(ans2);
			break;
			
			
			case "3":
				System.out.println("Digite el valor de la moneda: \n");
				String coinvalue = scanner.next();
				Integer coinintvalue = 0;
				try {
			       coinintvalue=Integer.parseInt(coinvalue);
			    } catch (NumberFormatException e) {
			         coinintvalue = 0;
			    }
				String r = client.ContarMonedasDe(coinintvalue);
				if(r.contains("Moneda Incorrecta"))
					System.out.println(r);
				else if (r.contains("Null return value"))
					System.out.println("No hay monedas de $"+coinvalue+" en la alcancía"); 
				else if (r.contains("error"))
					System.out.println(r);
				else
						System.out.println("Hay un total de "+r+" monedas de "+coinvalue+" en la alcancía");	
					break;
				
			case "4":
				String ans4 = "Hay un total de $"+client.TotalDinero()+" pesos en la alcancía";
				System.out.println(ans4);
				break;
				
			case "5":
				System.out.println("Digite la denominación de la moneda: \n");
				String coinvalue2 = scanner.next();
				Integer coinintvalue2 = 0;
				try {
					coinintvalue2=Integer.parseInt(coinvalue2);
			    } catch (NumberFormatException e) {
			         coinintvalue2 = 0;
			    }
				String r2 = client.TotalEnMonedasDe(coinintvalue2);
				if(r2.contains("Moneda Incorrecta"))
					System.out.println(r2);
					else if (r2.contains("Null return value"))
						System.out.println("No hay monedas de $"+coinvalue2+" en la alcancía"); 
					else if (r2.contains("error"))
						System.out.println(r2);
					else
						System.out.println("Hay un total de $"+r2+" pesos en monedas de "+coinvalue2+" en la alcancía");	
					break;
					
			case "6":
				System.exit(0);
					
				default:
					break;
					
		}
	   
		System.out.println("----------------------FIN---------------------");
		
		}
	scanner.close();
	}

}
