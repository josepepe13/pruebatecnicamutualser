package com.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//Esta es nuestra entidad moneda, igual que en la API REST
@JsonIgnoreProperties(ignoreUnknown = true)
public class moneda {
	
		
		private Integer Id;
		private Integer Valor;
		public Integer getId() {
			return Id;
		}
		public void setId(Integer id) {
			Id = id;
		}
		public Integer getValor() {
			return Valor;
		}
		public void setValor(Integer valor) {
			Valor = valor;
		}
}
