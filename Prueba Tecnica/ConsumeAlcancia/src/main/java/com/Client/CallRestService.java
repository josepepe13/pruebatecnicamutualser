package com.Client;


import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.model.moneda;

@Component
public class CallRestService {

	//Este es nuestro cliente REST, nuestro servidor y puerto esta quemado en codigo
	//Si ejecuta la api en un servidor remoto ud deberá cambiarlo en el primer parametro de la funcion restTemplate.exchange
	
	//Consumimos para guardar la moneda
	public String GuardarMoneda(moneda coin)
	{
	RestTemplate restTemplate = new RestTemplate();
	HttpEntity<moneda> entity = new HttpEntity<>(coin);
	 try{
		 return restTemplate.exchange("http://localhost:8080/monedas/guardar",HttpMethod.POST, entity,String.class).getBody();
	 }catch(Exception e) {return e.getMessage();}
	 }
	//Consumimos para contar las monedas
	public String ContarMonedas()
	{
	RestTemplate restTemplate = new RestTemplate();
	
	 try{
		 return restTemplate.exchange("http://localhost:8080/monedas/contarmonedas",HttpMethod.GET, null,String.class).getBody();
	 }catch(Exception e) {return e.getMessage();}
	 }
	//Consumimos para saber el total de dinero
	public String TotalDinero()
	{
	RestTemplate restTemplate = new RestTemplate();
	
	 try{
		 return restTemplate.exchange("http://localhost:8080/monedas/total",HttpMethod.GET, null,String.class).getBody();
	 }catch(Exception e) {return e.getMessage();}
	 }
	
	//Consumimos para contar las monedas de x denominacion
	public String ContarMonedasDe(Integer v)
	{
	RestTemplate restTemplate = new RestTemplate();
	moneda coin = new moneda();
	coin.setValor(v);
	HttpEntity<moneda> entity = new HttpEntity<>(coin);
	try {
		 return restTemplate.exchange("http://localhost:8080/monedas/contarmonedasde",HttpMethod.POST, entity,String.class).getBody();
	 }catch(Exception e) {return e.getMessage();}

	 }
	//Consumimos para saber el total en monedas de x denominacion
	public String TotalEnMonedasDe(Integer v)
	{
	RestTemplate restTemplate = new RestTemplate();
	moneda coin = new moneda();
	coin.setValor(v);
	HttpEntity<moneda> entity = new HttpEntity<>(coin);
	try {
		 return restTemplate.exchange("http://localhost:8080/monedas/totalenmonedasde",HttpMethod.POST, entity,String.class).getBody();
	 }catch(Exception e) {return e.getMessage();}

	 }
}
